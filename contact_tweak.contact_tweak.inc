<?php
/**
 * @file
 * Contact tweak field definitions.
 */

/**
 * Implements hook_contact_tweak_fields().
 */
function contact_tweak_contact_tweak_fields() {
  $fields = [];
  $fields['sender_name'] = [
    'field_type' => 'textfield',
    'form_map' => ['name'],
    'widget_depth' => [],
    'alter' => [
      'title' => TRUE,
      'value' => FALSE,
      'description' => TRUE,
      'default_value' => TRUE,
      'checked' => FALSE,
      'hidden' => TRUE,
    ],
  ];
  $fields['sender_mail'] = [
    'field_type' => 'email',
    'form_map' => ['mail'],
    'widget_depth' => [],
    'alter' => [
      'title' => TRUE,
      'value' => FALSE,
      'description' => TRUE,
      'default_value' => TRUE,
      'checked' => FALSE,
      'hidden' => TRUE,
    ],
  ];
  $fields['subject'] = [
    'field_type' => 'textfield',
    'form_map' => ['subject'],
    'widget_depth' => ['widget', 0, 'value'],
    'alter' => [
      'title' => TRUE,
      'value' => FALSE,
      'description' => TRUE,
      'default_value' => TRUE,
      'checked' => FALSE,
      'hidden' => TRUE,
    ],
  ];
  $fields['message'] = [
    'field_type' => 'textarea',
    'form_map' => ['message'],
    'widget_depth' => ['widget', 0, 'value'],
    'alter' => [
      'title' => TRUE,
      'value' => FALSE,
      'description' => TRUE,
      'default_value' => TRUE,
      'checked' => FALSE,
      'hidden' => TRUE,
    ],
  ];
  $fields['copy'] = [
    'field_type' => 'checkbox',
    'form_map' => ['copy'],
    'alter' => [
      'title' => TRUE,
      'value' => FALSE,
      'description' => TRUE,
      'default_value' => FALSE,
      'checked' => TRUE,
      'hidden' => TRUE,
    ],
  ];
  $fields['submit'] = [
    'field_type' => 'textfield',
    'form_map' => ['actions', 'submit'],
    'alter' => [
      'title' => FALSE,
      'value' => TRUE,
      'default_value' => FALSE,
      'checked' => FALSE,
      'hidden' => FALSE,
    ],
  ];
  return $fields;
}
