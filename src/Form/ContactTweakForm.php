<?php
/**
 * @file
 * Contains \Drupal\contact_tweak\Form\ContactTweakForm.
 */

namespace Drupal\contact_tweak\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\contact\ContactFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;

/**
 * Defines a form to configure maintenance settings for this site.
 */
class ContactTweakForm extends ConfigFormBase {

  protected $contactForm;

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'config_tweak_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['contact_tweak.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('contact_tweak.settings');
    $buildInfo = $form_state->getBuildInfo();
    $formId = $buildInfo['args'][0]['contactFormEntity']->id();

    $get_config = function($self, $field_name, $component) use ($config, $formId) {
      $configKey = implode('.', [
        $formId,
        $field_name,
        $component,
      ]);
      $config_value = $config->get($configKey);
      if (empty($config_value)) {
        return NULL;
      }
      return $config_value;
    };

    $fields = contact_tweak_fields();

    foreach ($fields as $field_name => $field_config) {
      $form[$field_name] = [
        '#type' => 'fieldset',
        '#title' => $field_name,
        '#tree' => TRUE,
      ];

      if (!empty($field_config['alter']['title'])) {
        $form[$field_name]['title'] = [
          '#type' => 'textfield',
          '#title' => t('Title'),
          '#default_value' => $get_config($this, $field_name, 'title'),
        ];
      }

      if (!empty($field_config['alter']['value'])) {
        $form[$field_name]['value'] = [
          '#type' => $field_config['field_type'],
          '#title' => t('Value'),
          '#default_value' => $get_config($this, $field_name, 'value'),
        ];
      }

      if (!empty($field_config['alter']['description'])) {
        $form[$field_name]['description'] = [
          '#type' => 'textarea',
          '#title' => t('Description'),
          '#default_value' => $get_config($this, $field_name, 'description'),
        ];
      }

      if (!empty($field_config['alter']['default_value'])) {
        $form[$field_name]['default_value'] = [
          '#type' => $field_config['field_type'],
          '#title' => t('Default Value'),
          '#default_value' => $get_config($this, $field_name, 'default_value'),
        ];
      }

      if (!empty($field_config['alter']['checked'])) {
        $form[$field_name]['checked'] = [
          '#type' => 'checkbox',
          '#title' => t('Checked'),
          '#checked_value' => 1,
          '#default_value' => $get_config($this, $field_name, 'checked'),
        ];
      }

      if (!empty($field_config['alter']['hidden'])) {
        $form[$field_name]['hidden'] = [
          '#type' => 'checkbox',
          '#title' => t('Hidden'),
          '#checked_value' => 1,
          '#default_value' => $get_config($this, $field_name, 'hidden'),
        ];
      }
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('contact_tweak.settings');
    $buildInfo = $form_state->getBuildInfo();
    $formId = $buildInfo['args'][0]['contactFormEntity']->id();

    $fields = contact_tweak_fields();
    foreach ($fields as $field_name => $field_config) {
      foreach ($field_config['alter'] as $alter_key => $allowed) {
        if (!!$allowed) {
          $configKey = implode('.', [
            $formId,
            $field_name,
            $alter_key,
          ]);
          $configValue = $form_state->getValue([$field_name, $alter_key]);
          $config->set($configKey, $configValue);
        }
      }
    }

    $config->save();
    Cache::invalidateTags(['config:core.entity_form_display.contact_message.' . $formId . '.default']);
    parent::submitForm($form, $form_state);
  }
}
